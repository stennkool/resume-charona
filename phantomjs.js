var page = require('webpage').create();
page.viewportSize = { width: 1024, height: 768 };
page.open('index.html', function() {
    setTimeout(function() {
        page.render('cv.pdf');
        phantom.exit();
    }, 1500);
});